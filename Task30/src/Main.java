import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String word1 = input.next();
        String word2 = input.next();
        System.out.println("Is the words you wrote are same: " + compareStrings(word1, word2));
    }
    public static boolean compareStrings (String word1, String word2) {
        if (word1.equalsIgnoreCase(word2)) {
            return true;
        }
        return false;
    }
}