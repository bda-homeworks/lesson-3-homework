import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Bir reqem daxil edin");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();

        if(num == 0) {
            System.out.println("0");
        } else if(num > 0) {
            System.out.println("+1");
        } else {
            System.out.println("-1");
        }
    }
}