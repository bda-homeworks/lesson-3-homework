import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Birinci reqemi daxil edin: ");
        double num1 = input.nextDouble();

        System.out.println("Ikinci reqemi daxil edin: ");
        double num2 = input.nextDouble();

        System.out.println("Operatoru daxil edin (+ - * /): ");
        char operator = input.next().charAt(0);

        double result = 0;

        switch (operator) {
            case '+':
                result = toplama(num1, num2);
                break;
            case '-':
                result = cixma(num1, num2);
                break;
            case '*':
                result = vurma(num1, num2);
                break;
            case '/':
                result = bolme(num1, num2);
                break;
        }
        System.out.println(num1 + " " + operator + " " + num2 + " = " + result);
    }
    public static double toplama(double num1, double num2) {
        return num1 + num2;
    }
    public static double cixma(double num1, double num2) {
        return num1 - num2;
    }
    public static double vurma(double num1, double num2) {
        return num1 * num2;
    }
    public static double bolme(double num1, double num2) {
        if (num2 == 0) {
            System.out.println("Error: 0-a bole bilmezsiz!");
            return 0;
        }
        return num1 / num2;
    }
}