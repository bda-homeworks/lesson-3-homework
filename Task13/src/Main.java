import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Reqem tipini qeyd edin");
        System.out.println("1. Integer");
        System.out.println("2. Double");
        System.out.println("3. Byte");
        int choice = input.nextInt();

        System.out.println("3 dene reqem qeyd edin birincisi secdiyiniz tip olmaqla:");
        if (choice == 1) {
            int num1 = input.nextInt();
            double num2 = input.nextDouble();
            byte num3 = input.nextByte();
            double sum = num1 + num2 + num3;
            System.out.println("3 ededin toplamasi double tipli olaraq " + sum + " oldu");
        } else if (choice == 2) {
            double num1 = input.nextInt();
            int num2 = input.nextInt();
            byte num3 = input.nextByte();
            double sum = (num1 + num2 + num3);
            System.out.println("3 ededin toplamasi double tipli olaraq " + sum + " oldu");
        } else if (choice == 3) {
            byte num1 = input.nextByte();
            int num2 = input.nextInt();
            double num3 = input.nextDouble();
            double sum = (num1 + num2 + num3);
            System.out.println("3 ededin toplamasi double tipli olaraq " + sum + " oldu");
        } else {
            System.out.println("Ele bir secim yoxdur!!");
        }
    }

}
