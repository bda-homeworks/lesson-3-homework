import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a decimal number: ");
        int decimal = input.nextInt();

        int octal = convertDecimalToOctal(decimal);

        System.out.println(decimal + " in decimal = " + octal + " in octal");
    }

    public static int convertDecimalToOctal(int decimal) {
        int octal = 0;
        int i = 1;

        while (decimal != 0) {
            int remainder = decimal % 8;
            decimal = decimal / 8;
            octal += remainder * i;
            i *= 10;
        }

        return octal;
    }
}