import java.util.Scanner;

public class SecondWay {
    public static void main(String[] args) {
        Scanner decimal = new Scanner(System.in);
        long num = decimal.nextLong();
        System.out.println(Long.toOctalString(num));
    }
}
