import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Birinci reqemi qeyd edin:");
        int num1 = input.nextInt();

        System.out.println("Ikinci reqemi qeyd edin:");
        int num2 = input.nextInt();

        System.out.println("Ucuncu reqemi qeyd edin:");
        int num3 = input.nextInt();

        int highest = num1;
        if (num2 > highest) {
            highest = num2;
        }
        if (num3 > highest) {
            highest = num3;
        }

        System.out.println("En yuksek reqem: " + highest);
    }
}