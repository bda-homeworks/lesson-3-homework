public class Main {
    public static void main(String[] args) {
        long startTime, endTime, totalTime;
        startTime = System.nanoTime();
        int x = 5;
        switch (x) {
            case 1:
                System.out.println("Case 1");
                break;
            case 2:
                System.out.println("Case 2");
                break;
            case 3:
                System.out.println("Case 3");
                break;
            case 4:
                System.out.println("Case 4");
                break;
            case 5:
                System.out.println("Case 5");
                break;
            default:
                System.out.println("Default case");
                break;
        }
        endTime = System.nanoTime();
        totalTime = endTime - startTime;
        System.out.println("Birinci Switch umimi vaxt serfiyyati: " + totalTime + " nanoseconds");

        startTime = System.nanoTime();
        int y = 5;
        if (y == 1) {
            System.out.println("If 1");
        } else if (y == 2) {
            System.out.println("If 2");
        } else if (y == 3) {
            System.out.println("If 3");
        } else if (y == 4) {
            System.out.println("If 4");
        } else if (y == 5) {
            System.out.println("If 5");
        } else {
            System.out.println("Default if");
        }
        endTime = System.nanoTime();
        totalTime = endTime - startTime;
        System.out.println("If metodunun vaxt serfiyati: " + totalTime + " nanoseconds");
    }
}