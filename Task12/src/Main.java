import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Menfi ve ya Musbet oldugunu yoxlamaq istediyiniz ededi qeyd edin:");
        Scanner input = new Scanner(System.in);
        int num = input.nextInt();

        if (num > 0) {
            System.out.println(num + " Qeyd etdiyiniz reqem musbetdir");
        } else {
            System.out.println(num + " Qeyd etdiyiniz reqem menfidir");
        }
    }
}