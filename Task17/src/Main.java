import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num1 = input.nextInt();

        if (num1 % 2 == 0 && num1 > 100) {
            System.out.println("Ugurlu emeliyyat!");
        } else {
            System.out.println("Ugursuz emeliyyat :(");
        }
    }
}