import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Write 3 words to compare is any of them is same with last word you wrote.");
        String word1 = input.nextLine();
        String word2 = input.nextLine();
        String word3 = input.nextLine();
        System.out.println(method(word1, word2, word3));
    }
    public static boolean method(String a, String b, String c) {
        String[] arr = {a,b};
        boolean check = false;
        for (int i =0; i< arr.length; i++) {
            if (arr[i].equalsIgnoreCase(c)) {
                check = true;
            }
        }
        return check;
    }
}
