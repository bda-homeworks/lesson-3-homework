import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int var1 = scanner.nextInt();
        int var2 = scanner.nextInt();

        boolean boolValue = scanner.nextBoolean();

        if (boolValue) {
            System.out.println(multiply(var1, var2));
        } else {
            System.out.println(difference(var1, var2));
        }
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static int difference(int a, int b) {
        return a + b;
    }
}